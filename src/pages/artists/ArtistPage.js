/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { View } from 'react-native';
import { Avatar, Text, SearchBar } from 'react-native-elements';

import { getArtistList, clearArtistList } from '../../modules/actions/artistActions';
import PageContainer from '../../components/page-container/PageContainer';
import PageContainerInner from '../../components/page-container/PageContainerInner';
import Spinner from '../../components/spinner/Spinner';
import { THEME, COLORS } from '../../constants';

class LoginPage extends Component {
  state = {
    searchText: '',
  }

  render() {
    const { isLoading } = this.props.artists;
    return (
      <PageContainer style={pageStyles.container}>
        {this.renderSearchBar()}

        <PageContainerInner scroll style={pageStyles.innerContainer}>
          {isLoading && this.renderSpinner()}
          {this.renderList()}
        </PageContainerInner>

      </PageContainer>
    );
  }

  componentDidMount() {
    this.search.focus();
  }

  renderSpinner = () => (
      <View style={pageStyles.spinnerContainer}>
        <Spinner />
      </View>
    )

  renderSearchBar = () => {
    const { searchText } = this.state;
    const compText = {
      placeHolder: 'Type an Artist Name Here...',
      search: 'Search',
    };
    return (
      <View style={pageStyles.searchBarContainer}>
        <SearchBar
          value={searchText}
          lightTheme
          searchIcon
          autoCorrect={false}
          ref={search => this.search = search}
          placeholder={compText.placeHolder}
          containerStyle={pageStyles.searchBar}
          clearIcon={{ color: COLORS.SUBTEXT }}
          onChangeText={this.handleSearchChange}
          onSubmitEditing={this.handleSearchSubmit}
          onClear={this.handleClearSearch}
        />
      </View>
    );
  };

  renderList = () => {
    const { artistListCount } = this.props.artists;
    return (
      <View>
        {artistListCount ? this.renderListItems() : this.renderEmptyState()}
      </View>
    );
  };

  renderEmptyState = () => {
    const { isLoading } = this.props.artists;
    const compText = {
      title: 'Search For An Artist.',
      desc: 'Enter the name of the artist in the search box above.',
    };

    if (isLoading) return <View />;

    return (
      <View style={pageStyles.emptyStateContainer}>
        <Text h4 style={pageStyles.emptyStateTitle}>{compText.title}</Text>
        <Text style={pageStyles.emptyStateDesc}>{compText.desc}</Text>
      </View>
    );
  }

  renderListItems = () => {
    const { artistList, isLoading } = this.props.artists;
    return artistList.map((item, index) => {
      const {
        artistName,
        trackName,
        artworkUrl100,
        primaryGenreName,
        releaseDate,
      } = item;
      const isPriceVisible = this.handlePrice(item);
      const formattedRelease = `Released ${moment(releaseDate).format('ll')}`;

      if (isLoading) return <View />;

      return (
        <View key={index} style={pageStyles.itemContainer}>
          <View style={pageStyles.itemAlbumContainer}>
            <Avatar
              source={{ uri: artworkUrl100 }}
              size={'xlarge'}
            />
          </View>
          <View style={pageStyles.itemDetailsContainerPri}>
            <Text style={pageStyles.itemTextTitle}>{trackName}</Text>
            <Text>{artistName}</Text>

            <View style={pageStyles.itemDetailsContainerSec}>
              <Text style={pageStyles.itemTextSubTitle}>{primaryGenreName}</Text>
              <Text style={pageStyles.itemTextSubTitle}>{formattedRelease}</Text>
              {isPriceVisible && <Text style={[pageStyles.itemTextSubTitle, pageStyles.itemPrice]}>${this.handlePrice(item)}</Text>}
            </View>
          </View>

        </View>
      );
    });
  };

  handlePrice = (item) => {
    const {
      kind,
      trackPrice,
      collectionPrice,
    } = item;
    let price = '';

    switch (kind) {
      case 'song':
      case 'feature-movie':
        price = trackPrice;
        break;
      case 'podcast':
        price = null;
        break;
      case 'audiobook':
        price = collectionPrice;
        break;
      default:
        price = null;
    }
    return price;
  }

  handleSearchChange=(searchText) => {
    this.setState({ searchText });
  }

  handleSearchSubmit = () => {
    const { getArtistList } = this.props;
    const { searchText } = this.state;

    getArtistList(searchText)
      .then()
      .catch();
  }

  handleClearSearch = () => {
    const { clearArtistList } = this.props;
    clearArtistList();
  }
}

const pageStyles = {
  container: {
    paddingTop: THEME.MARGIN * 3,
  },
  innerContainer: {
    paddingHorizontal: THEME.MARGIN,
  },
  spinnerContainer: {
    marginTop: THEME.MARGIN * 7,
  },
  searchBarContainer: {
    flexDirection: 'row',
  },
  emptyStateContainer: {
    flex: 1,
    marginTop: THEME.MARGIN * 5,
    width: '80%',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  emptyStateTitle: {
    textAlign: 'center',
    marginBottom: THEME.MARGIN,
  },
  emptyStateDesc: {
    textAlign: 'center',
    paddingHorizontal: THEME.PADDING,
  },
  searchBar: {
    flex: 1,
    backgroundColor: COLORS.BG_GRAY,
  },
  input: {
    width: '80%',
  },
  itemContainer: {
    flexDirection: 'row',
    marginVertical: THEME.MARGIN,
    alignItems: 'flex-start',
  },
  itemAlbumContainer: {
    backgroundColor: 'yellow',
  },
  itemDetailsContainerPri: {
    flex: 1,
    paddingHorizontal: THEME.MARGIN,
  },
  itemDetailsContainerSec: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  itemTextTitle: {
    fontSize: THEME.FONT_SIZE_SUBTITLE - 4,
    fontWeight: 'bold',
  },
  itemTextSubTitle: {
    fontSize: THEME.FONT_SIZE_PARAGRAPH - 4,
    opacity: 0.7,
  },
  itemPrice: {
    padding: THEME.MARGIN / 3,
    borderWidth: THEME.BORDER_WIDTH,
    borderColor: COLORS.PRIMARY,
    borderRadius: THEME.BORDER_RADIUS,
    color: COLORS.PRIMARY,
    flex: 0,
    marginTop: THEME.MARGIN,
    maxWidth: 80,
    textAlign: 'center',
  },
};

const mapStoreToProps = ({ artists }) => ({
  artists,
});

const mapActionsToProps = {
  getArtistList,
  clearArtistList,
};

export default connect(mapStoreToProps, mapActionsToProps)(LoginPage);

