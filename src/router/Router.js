import React, { Component } from 'react';
import { Platform } from 'react-native';
import { Stack, Scene, Router, Modal } from 'react-native-router-flux';

import ROUTE from '../router/routes';
import { COLORS, THEME } from '../constants';
import ArtistPage from '../pages/artists/ArtistPage';


export default class RouterComponent extends Component {
    render() {
        return (
            <Router
                sceneStyle={styles.navScene}
                navigationBarStyle={styles.navBar}
                titleStyle={styles.navTitle}
                backButtonTextStyle={styles.navButtonText}
                leftButtonTextStyle={styles.navButtonText}
                leftButtonIconStyle={styles.backButton}
                rightButtonTextStyle={styles.navButtonText}
            >
                <Modal
                    hideNavBar
                >
                    <Stack key="root" hideNavBar>
                        {this.renderAuthScene()}
                    </Stack>

                </Modal>

            </Router>
        );
    }

    renderAuthScene = () => (
        <Stack
            modal
            key={ROUTE.ARTISTS.key}

        >
            <Scene
                key={ROUTE.ARTISTS.key}
                title={ROUTE.ARTISTS.title}
                component={ArtistPage}
                panHandlers={null}
                animation={'fade'}
                hideNavBar
                initial
            />

        </Stack>
    )
}

const styles = {
    backButtonText: {
        color: 'white',
        flex: 0,
        alignSelf: 'center',
    },
    backButton: {
        marginBottom: 0,
        alignItems: 'flex-start',
        minWidth: 30,

        ...Platform.select({
            ios: {
                minHeight: '100%',
            },
        }),
    },
    navBar: {
        backgroundColor: COLORS.PRIMARY,
        borderBottomWidth: 0,
    },

    navScene: {
        backgroundColor: COLORS.BG1,
    },

    navTitle: {
        color: COLORS.NAV_TEXT,
        fontWeight: 'bold',
        fontSize: 17,
    },

    tabBar: {
        backgroundColor: '#fafafa',
        height: THEME.NAV_HEIGHT,
        borderTopColor: COLORS.BORDER_COLOR,
        borderTopWidth: THEME.BORDER_WIDTH,
    },

    navButtonText: {
        color: COLORS.NAV_TEXT,
    },

    tabIconContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: 80,
    },

    tabIcon: {
        fontSize: 10,
    },

};

