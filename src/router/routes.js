import { Actions } from 'react-native-router-flux';

const ROUTE = {
    ARTISTS: {
        title: 'Artists',
        key: 'artists',
        icon: '',
        route: () => Actions.artists(),
    },

};

export default ROUTE;
