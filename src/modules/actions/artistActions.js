import axios from 'react-native-axios';
import {
    GET_ARTIST_LIST_REQUEST,
  GET_ARTIST_LIST_SUCCESS,
  GET_ARTIST_LIST_FAILURE,
  CLEAR_ARTIST_LIST,
} from '../types/artistTypes';

export const getArtistList = artistName => (dispatch) => {
    dispatch({ type: GET_ARTIST_LIST_REQUEST });

    return new Promise((resolve, reject) => {
        axios.get(`https://itunes.apple.com/search?term=${artistName}`)
            .then((res) => {
                dispatch({
                    type: GET_ARTIST_LIST_SUCCESS,
                    results: res.data.results,
                    resultCount: res.data.resultCount,
                });
                return resolve();
            })
            .catch((err) => {
                dispatch({
                    type: GET_ARTIST_LIST_FAILURE,
                    payload: err.message,
                });
                return reject(err);
            });
    });
};

export const clearArtistList = () => (dispatch) => {
  dispatch({ type: CLEAR_ARTIST_LIST });
};

