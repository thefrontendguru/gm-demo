import {
  GET_ARTIST_LIST_REQUEST,
  GET_ARTIST_LIST_SUCCESS,
  GET_ARTIST_LIST_FAILURE,
  CLEAR_ARTIST_LIST,
} from '../types/artistTypes';

const INITIAL_STATE = {
  artistList: [],
  artistListCount: 0,
  isLoading: false,
  error: '',
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_ARTIST_LIST_REQUEST:
      return { ...state, isLoading: true };
    case GET_ARTIST_LIST_SUCCESS:
      return { ...state, isLoading: false, artistList: action.results, artistListCount: action.resultCount, error: '' };
    case GET_ARTIST_LIST_FAILURE:
      return { ...state, isLoading: false, error: action.error };

    case CLEAR_ARTIST_LIST:
      return { ...INITIAL_STATE };

    default:
      return state;
  }
};

export default reducer;
