// Store Dependencies
import { createStore, applyMiddleware, combineReducers } from 'redux';
import ReduxThunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

// Reducers
import artistReducer from './reducers/artistReducer';

// Redux Store Schema
const rootReducer = combineReducers({
  artists: artistReducer,
});

const store = createStore(
  rootReducer,
  {},
  composeWithDevTools(
    applyMiddleware(ReduxThunk),
  ),
);

export default store;
