const THEME = {
    BORDER_RADIUS: 5,
    BORDER_WIDTH: 1,
    NAV_HEIGHT: 56,
    MARGIN: 15,
    PADDING: 15,

    FONT_SIZE_TITLE: 24,
    FONT_SIZE_SUBTITLE: 20,
    FONT_SIZE_SUB_SUBTITLE: 18,
    FONT_SIZE_PARAGRAPH: 16,
};

const COLORS = {
    PRIMARY: '#00274d',
    SECONDARY: '#00698f',
    TERTIARY: '#5c8727',

    ERROR: '#d0011b',
    SUCCESS: '#507a22',
    WARNING: '#E8B800',

    TEXT: '#4a4a4a',
    SUBTEXT: '#9b9b9b',
    LINK: '#0076ff',
    TEXT_PLACEHOLDER: '#9b9b9b',

    BG1: '#fff',
    BG2: '#cdcdcd',
    BG_GRAY: '#f4f4f4',

    BUTTON: '#4990e2',
    BUTTON_TEXT: '#fff',

    BORDER_COLOR: '#d0d0d0',

    NAV_TEXT: '#fff',
};


export default COLORS;

export { THEME, COLORS };
