/* eslint-disable import/prefer-default-export */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, ActivityIndicator, Text } from 'react-native';
import { COLORS, THEME } from '../../constants';

export default class Spinner extends Component {
    static get propTypes() {
        return {
            text: PropTypes.string,
            size: PropTypes.string,
            color: PropTypes.string,
        };
    }

    static get defaultProps() {
        return {
            size: 'large',
            color: 'dark', // accepts 'dark', 'light', or hex '#fffff'
        };
    }

    render() {
        const { size, text } = this.props;
        return (
            <View style={styles.spinnerStyle}>
                {!!text && <Text style={[styles.text, { color: this.renderColor() }]}>{text}</Text>}
                <ActivityIndicator size={size} color={this.renderColor()} />
            </View>
        );
    }

    renderColor = () => {
        const { color } = this.props;

        if (color === 'dark') {
            return COLORS.PRIMARY;
        } else if (color === 'light') {
            return '#fff';
        }
            return color;
    }
}

const styles = {
    text: {
        marginBottom: THEME.MARGIN,
        fontSize: THEME.FONT_SIZE_SUB_SUBTITLE,
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        minHeight: 50,
    },
    spinnerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
};

