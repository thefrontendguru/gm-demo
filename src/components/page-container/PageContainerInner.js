import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, KeyboardAvoidingView } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { THEME } from '../../constants';

export default class PageContainerInner extends Component {
    static get propTypes() {
        return {
            children: PropTypes.any,
            scroll: PropTypes.bool,
            style: PropTypes.object,
            hasPadding: PropTypes.bool,
        };
    }

    static get defaultProps() {
        return {
            scroll: false,
        };
    }

    render() {
        const { scroll } = this.props;
        return scroll ? this.renderScrollView() : this.renderView();
    }

    renderView = () => {
        const { style, hasPadding } = this.props;
        const paddingStyles = hasPadding ? styles.padding : null;
        return (
            <KeyboardAvoidingView behavior="padding" style={[styles.container, paddingStyles, style]}>
                {this.props.children}
            </KeyboardAvoidingView>
        );
    }

    renderScrollView = () => {
        const { style, children } = this.props;
        return (
            <KeyboardAwareScrollView
                extraScrollHeight={100}
                enableOnAndroid
                keyboardShouldPersistTaps={'always'}
            >
                <View style={[styles.container, style]}>
                    {children}
                </View>
            </KeyboardAwareScrollView>
        );
    }
}

const styles = {
    container: {
        flex: 1,
    },

    padding: {
        paddingLeft: THEME.MARGIN,
        paddingRight: THEME.MARGIN,
    },
};
