import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { COLORS } from '../../constants';

class PageContainer extends Component {
    static get propTypes() {
        return {
            children: PropTypes.any,
            style: PropTypes.object,
            hasNav: PropTypes.bool,
        };
    }

    render() {
        const { hasNav, style } = this.props;
        const hasNavCls = hasNav ? styles.hasNav : '';
        return (
            <View style={[styles.container, style, hasNavCls]}>
                {this.props.children}
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        flexDirection: 'column',
        alignSelf: 'stretch',
        backgroundColor: COLORS.BG_GRAY,
    },
};

const mapStoreToProps = ({ network }) => ({
    network,
});

const mapActionsToProps = {};

export default connect(mapStoreToProps, mapActionsToProps)(PageContainer);
