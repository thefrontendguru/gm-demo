import React from 'react';
import {shallow} from 'enzyme';
import Icon from "../Icon";
import EntypoIcon from 'react-native-vector-icons/Entypo';
import FeatherIcon from 'react-native-vector-icons/Feather';
import EvilIcon from 'react-native-vector-icons/EvilIcons';
import IonIcon from 'react-native-vector-icons/Ionicons';
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

describe('Icon component', () => {

	let props;

	beforeEach(() => {
		props = {
			iconFamily: 'FontAwesome',
			iconName: 'thumbs-o-up',
			testID: 'icon'
		}
	});

	it('should render', () => {
		const component = shallow(<Icon {...props} />);
		expect(component.exists()).toBe(true);
		expect(component.findWhere(node => node.prop('testID') === 'icon').exists()).toBe(true);
	});

	it('renders Entypo icon', () => {

		props = {
			...props,
			iconFamily: 'Entypo',
			iconName: 'thumbs-down'
		}

		const component = shallow(<Icon {...props} />);
		expect(component.instance().props.iconFamily).toEqual(props.iconFamily);
		expect(component.instance().props.iconName).toEqual(props.iconName);
		expect(component.exists()).toBe(true);
		expect(component.findWhere(node => node.prop('testID') === 'icon').exists()).toBe(true);
		expect(component.find(EntypoIcon).exists()).toBe(true);
	});

	it('renders Feather icon', () => {

		props = {
			...props,
			iconFamily: 'Feather',
			iconName: 'thumbs-down'
		}

		const component = shallow(<Icon {...props} />);
		expect(component.instance().props.iconFamily).toEqual(props.iconFamily);
		expect(component.instance().props.iconName).toEqual(props.iconName);
		expect(component.exists()).toBe(true);
		expect(component.findWhere(node => node.prop('testID') === 'icon').exists()).toBe(true);
		expect(component.find(FeatherIcon).exists()).toBe(true);
	});

	it('renders Evil icon', () => {

		props = {
			...props,
			iconFamily: 'EvilIcons',
			iconName: 'minus'
		}

		const component = shallow(<Icon {...props} />);
		expect(component.instance().props.iconFamily).toEqual(props.iconFamily);
		expect(component.instance().props.iconName).toEqual(props.iconName);
		expect(component.exists()).toBe(true);
		expect(component.findWhere(node => node.prop('testID') === 'icon').exists()).toBe(true);
		expect(component.find(EvilIcon).exists()).toBe(true);
	});

	it('renders Ion icon', () => {

		props = {
			...props,
			iconFamily: 'IonIcons',
			iconName: 'ios-thumbs-down'
		}

		const component = shallow(<Icon {...props} />);
		expect(component.instance().props.iconFamily).toEqual(props.iconFamily);
		expect(component.instance().props.iconName).toEqual(props.iconName);
		expect(component.exists()).toBe(true);
		expect(component.findWhere(node => node.prop('testID') === 'icon').exists()).toBe(true);
		expect(component.find(IonIcon).exists()).toBe(true);
	});

	it('renders Simple Line icon', () => {

		props = {
			...props,
			iconFamily: 'SimpleLineIcons',
			iconName: 'vector'
		}

		const component = shallow(<Icon {...props} />);
		expect(component.instance().props.iconFamily).toEqual(props.iconFamily);
		expect(component.instance().props.iconName).toEqual(props.iconName);
		expect(component.exists()).toBe(true);
		expect(component.findWhere(node => node.prop('testID') === 'icon').exists()).toBe(true);
		expect(component.find(SimpleLineIcon).exists()).toBe(true);
	});

	it('renders Material Community icon', () => {

		props = {
			...props,
			iconFamily: 'MaterialCommunityIcons',
			iconName: 'thumbs-up-down'
		}

		const component = shallow(<Icon {...props} />);
		expect(component.instance().props.iconFamily).toEqual(props.iconFamily);
		expect(component.instance().props.iconName).toEqual(props.iconName);
		expect(component.exists()).toBe(true);
		expect(component.findWhere(node => node.prop('testID') === 'icon').exists()).toBe(true);
		expect(component.find(MaterialCommunityIcon).exists()).toBe(true);
	});
});
