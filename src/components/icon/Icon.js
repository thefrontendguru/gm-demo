import React, {Component} from 'react';
import {View, ViewPropTypes} from 'react-native';
import PropTypes from 'prop-types';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesomeFive from 'react-native-vector-icons/FontAwesome5';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Foundation from 'react-native-vector-icons/Foundation';
import IonIcons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Octicons from 'react-native-vector-icons/Octicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Zocial from 'react-native-vector-icons/Zocial';

import {THEME} from '../../constants';

const ICONFAMILY = {
	antDesign: 'AntDesign',
	entypo: 'Entypo',
	evilIcons: 'EvilIcons',
	feather: 'Feather',
	fontAwesome: 'FontAwesome',
	fontAwesome5: 'FontAwesome5',
	fontisto: 'Fontisto',
	foundation: 'Foundation',
	ionIcons: 'IonIcons',
	materialIcons: 'MaterialIcons',
	materialComIcons: 'MaterialCommunityIcons',
	octicons: 'Octicons',
	simpleLineIcons: 'SimpleLineIcons',
	zocial: 'Zocial',
};

class Icon extends Component {

	static propTypes = {
		iconName: PropTypes.string.isRequired,
		iconFamily: PropTypes.oneOf([
			'AntDesign',
			'Entypo',
			'EvilIcons',
			'Feather',
			'Fontisto',
			'FontAwesome',
			'FontAwesomeFive',
			'Foundation',
			'IonIcons',
			'MaterialCommunityIcons',
			'MaterialIcons',
			'Octicons',
			'SimpleLineIcons',
			'Zocial',
		]).isRequired,
		iconColor: PropTypes.string,
		size: PropTypes.number,
		style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
		containerStyle: ViewPropTypes.style,
		testID: PropTypes.string
	};

	render() {
		const {containerStyle, testID} = this.props;
		return (
			<View
				style={[styles.container, containerStyle]}
				testID={testID}
			>
				{this.renderIcon()}
			</View>
		);
	}

	renderIcon = () => {
		const {style, iconName, iconFamily, iconColor, size} = this.props;
		let iconComponent = <View />;
		const iconSize = size ? size : styles.iconFontSize.fontSize * 2;
		const iconProps = {
			name: iconName,
			size: iconSize,
			color: iconColor,
			style
		};
		switch (iconFamily) {
			case ICONFAMILY.antDesign:
				iconComponent = <AntDesign {...iconProps} />;
				break;
			case ICONFAMILY.entypo:
				iconComponent = <Entypo {...iconProps} />;
				break;
			case ICONFAMILY.evilIcons:
				iconComponent = <EvilIcons {...iconProps} />;
				break;
			case ICONFAMILY.feather:
				iconComponent = <Feather {...iconProps} />;
				break;
			case ICONFAMILY.fontAwesome:
				iconComponent = <FontAwesome {...iconProps} />;
				break;
			case ICONFAMILY.fontAwesome5:
				iconComponent = <FontAwesomeFive {...iconProps} />;
				break;
			case ICONFAMILY.fontisto:
				iconComponent = <Fontisto {...iconProps} />;
				break;
			case ICONFAMILY.foundation:
				iconComponent = <Foundation {...iconProps} />;
				break;
			case ICONFAMILY.ionIcons:
				iconComponent = <IonIcons {...iconProps} />;
				break;
			case ICONFAMILY.materialIcons:
				iconComponent = <MaterialIcons {...iconProps} />;
				break;
			case ICONFAMILY.materialComIcons:
				iconComponent = <MaterialCommunityIcons {...iconProps} />;
				break;
			case ICONFAMILY.octicons:
				iconComponent = <Octicons {...iconProps} />;
				break;
			case ICONFAMILY.simpleLineIcons:
				iconComponent = <SimpleLineIcons {...iconProps} />;
				break;
			case ICONFAMILY.zocial:
				iconComponent = <Zocial {...iconProps} />;
				break;

		}

		return iconComponent;

	}
}

const styles = {
	iconFontSize: {
		fontSize: THEME.FONT_SIZE_SUB_SUBTITLE
	},
	container: {
		flexDirection: 'row',
		alignItems: 'center'
	},

};

export default Icon;
export {ICONFAMILY};