import React from 'react';
import { Provider } from 'react-redux';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import store from './modules/store';
import Router from './router/Router';

export default class App extends React.Component {
  render() {
    return (
      <SafeAreaProvider>
        <Provider store={store}>
          <Router />
        </Provider>
      </SafeAreaProvider>
    );
  }
}

