
# Getting started

### Install dependencies
If this is your first time running this project, i.e if you just cloned the project, run the command below to install deps and run the project with Ios

````
yarn bootstrap

````

### Launch Project on Ios

````
yarn ios
````

### Launch Project on Android

Coming soon

### Run Unit Tests
Current only one test exists.
````
yarn test
````
